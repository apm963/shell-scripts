#!/bin/bash

# Color list here: http://misc.flogisoft.com/bash/tip_colors_and_formatting
#colorS=( "\e[31m" "\e[32m" "\e[33m" "\e[34m" "\e[35m" "\e[36m" "\e[37m" "\e[90m" "\e[91m" "\e[92m" "\e[93m" "\e[94m" "\e[95m" "\e[96m" )
colorS=( "\e[31m\e[49m" "\e[32m\e[49m" "\e[33m\e[49m" "\e[34m\e[49m" "\e[35m\e[49m" "\e[36m\e[49m" "\e[37m\e[49m" "\e[90m\e[49m" "\e[91m\e[49m" "\e[92m\e[49m" "\e[93m\e[49m" "\e[94m\e[49m" "\e[95m\e[49m" "\e[96m\e[49m" "\e[39m\e[41m" "\e[39m\e[42m" "\e[39m\e[43m" "\e[39m\e[44m" "\e[39m\e[45m" "\e[39m\e[46m" "\e[39m\e[100m" "\e[39m\e[101m" "\e[39m\e[102m" "\e[39m\e[103m" "\e[39m\e[104m" "\e[39m\e[105m" "\e[39m\e[106m" )

colorLen=${#colorS[@]}
range=`expr ${colorLen} - 1`

str=$(ls $1)

IFS=$'\n'

ary=($str)

for key in "${!ary[@]}"
do
	colorI=`shuf -i 0-${range} -n 1`
	echo -e -n "${colorS[$colorI]}${ary[$key]}\e[0m "
done

echo ""
